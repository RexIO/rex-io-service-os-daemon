#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::Command;

use strict;
use warnings;

sub create {
   my ($class, $type, $app) = @_;

   my $klass = "Rex::IO::Service::OS::Daemon::Command::\u$type";

   eval "use $klass;";

   if($@) {
      die("Error loading class: $klass!");
   }

   return $klass->new(app => $app);
}

1;
