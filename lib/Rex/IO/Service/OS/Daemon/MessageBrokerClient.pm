#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::MessageBrokerClient;

use strict;
use warnings;

use Data::Dumper;
use Mojo::UserAgent;
use Mojo::IOLoop;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   $self->{ua} = Mojo::UserAgent->new;
   $self->ua->inactivity_timeout(0);
   $self->ua->request_timeout(0);

   $self->_ws("/messagebroker");

   return $self;
}

sub send {
   my ($self, $message, $code) = @_;

   if($code) {
      $self->tx->on(message => $code);
   }

   $self->tx->send($self->app->json->encode($message));

}

sub subscribe {
   my ($self, $code) = @_;
   $self->tx->on(message => sub {
      my ($tx, $msg) = @_;
      $self->app->log->debug("subscribe / got message: $msg");
      &$code($msg);
   });
}

sub ua  { (shift)->{ua}; }
sub tx  { (shift)->{tx}; }
sub app { (shift)->{app}; }

sub _ws {
   my ($self, $url) = @_;

   $self->ua->websocket("ws://" . $self->{server} . ":" . $self->{port} . $url => sub {
      my ($ua, $tx) = @_;
      if(! $tx->is_websocket) {
         $self->app->log->error("Can't connect to messagebroker");
         return 0;
      }
      else {
         $self->app->log->debug("Connected to messagebroker");
         $self->{tx} = $tx;
      }
   });
}


1;
