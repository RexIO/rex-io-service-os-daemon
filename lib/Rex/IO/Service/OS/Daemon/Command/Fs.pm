#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:

package Rex::IO::Service::OS::Daemon::Command::Fs;

use strict;
use warnings;

use Rex::IO::Service::OS::Daemon::Command::Base;
use base qw(Rex::IO::Service::OS::Daemon::Command::Base);

use Mojo::IOLoop;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   return $self;
}

sub run {
   my ($self, $opt) = @_;

   my $what = $opt->{fs};

   if($what eq "ls") {
      return $self->ls($opt->{location});
   }
}

sub ls {
   my ($self, $dir) = @_;

   my @ret;
   opendir(my $dh, $dir) or return [];
   while(my $entry = readdir($dh)) {
      next if($entry =~ m/^\./);

      my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$size,
                      $atime,$mtime,$ctime,$blksize,$blocks) = stat("$dir/$entry");

      push(@ret, {
         dev => $dev,
         ino => $ino,
         mode => $mode,
         nlink => $nlink,
         uid => $uid,
         gid => $gid,
         rdev => $rdev,
         size => $size,
         atime => $atime,
         mtime => $mtime,
         ctime => $ctime,
         blksize => $blksize,
         blocks => $blocks,
         name => $entry,
         dir => $dir,
         is_dir => (-d "$dir/$entry" ? 1 : 0),
      });
   }
   closedir($dh);

   return \@ret;
}

1;
