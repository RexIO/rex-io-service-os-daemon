#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::Command::Inventory;

use strict;
use warnings;

use Rex::IO::Service::OS::Daemon::Command::Base;
use base qw(Rex::IO::Service::OS::Daemon::Command::Base);

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = $proto->SUPER::new(@_);

   bless($self, $proto);

   return $self;
}

sub run {
   my ($self, $opt) = @_;

   # fire and forget
   system("fusioninventory-agent --server http://" . $self->{app}->{rexio_server} . "/fusioninventory");
}

1;
