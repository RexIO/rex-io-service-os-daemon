#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::Command::Monitor;

use strict;
use warnings;

use Rex::IO::Service::OS::Daemon::Command::Base;
use base qw(Rex::IO::Service::OS::Daemon::Command::Base);

use Mojo::IOLoop;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   return $self;
}

sub run {
   my ($self, $opt) = @_;
   print "Got welcome message: " . $opt->{welcome} . "\n";
}

sub start {
   my ($self) = @_;


   for my $m (@{ $self->app->config->{monitor}->{load} }) {
      my $name = $m;
      $name =~ s/\.(.)/::\u$1/g;
      my $klass = "Rex::IO::Service::OS::Daemon::Monitor::\u$name";
      $self->app->log->debug("Loading monitor: $klass");
      eval "use $klass";
      if($@) {
         $self->app->log->error("Loading monitor: $klass\n$@");
      }
   }

   $self->{timer} = Mojo::IOLoop->recurring($self->app->config->{monitor}->{interval} => sub {

      my $uuid = $self->get_uuid;

      my $monitoring_data = { type => "monitor", data => [
         {
            type => "single",
            name => "alive",
            value => 1,
         },
      ]};

      for my $m (@{ $self->app->config->{monitor}->{load} }) {
         my $name = $m;
         $name =~ s/\.(.)/::\u$1/g;
         my $klass = "Rex::IO::Service::OS::Daemon::Monitor::\u$name";

         my $monitor = $klass->new(app => $self->app);
         my $ret = $monitor->run;

         if(ref $ret) {
            push(@{$monitoring_data->{data}}, {
               type => "multi",
               values => $ret,
            });
         }
         else {
            if($ret eq "NOT_SUPPORTED") {
               $self->app->log->error("Got $ret from $m");
               next;
            }

            push(@{$monitoring_data->{data}}, {
               type => "single",
               name => $m,
               value => $ret,
            });
         }
      }

      $self->app->rexio->send($monitoring_data);

   });   
}

sub get_uuid {
   my ($self) = @_;
   return "<uuid>";
}

1;
