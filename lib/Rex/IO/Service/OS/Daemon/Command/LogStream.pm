#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:

package Rex::IO::Service::OS::Daemon::Command::LogStream;

use strict;
use warnings;

use Rex::IO::Service::OS::Daemon::Command::Base;
use base qw(Rex::IO::Service::OS::Daemon::Command::Base);

use Data::Dumper;
use Mojo::IOLoop;
use POSIX;

my $mon_map = {
   Jan => 0,
   Feb => 1,
   Mar => 2,
   Apr => 3,
   May => 4,
   Jun => 5,
   Jul => 6,
   Aug => 7,
   Sep => 8,
   Oct => 9,
   Nov => 10,
   Dec => 11,
};

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   return $self;
}

sub run {
   my ($self, $opt) = @_;
}

sub start {
   my ($self) = @_;

   my @pids = ();

   for my $log_action (@{ $self->app->config->{logstream} }) {

      my @cmd = ("tail", "-f", "-n", "500", $log_action->{file});
      my ($fh, $pid);

      $pid = open($fh, "-|", @cmd) or die("fork failed: $!");
      push(@pids, $pid);

      my $stream = Mojo::IOLoop::Stream->new($fh)->timeout(0);
      my $stream_id = Mojo::IOLoop->stream($stream);

      $stream->on(read => sub {
         my ($stream, $chunk) = @_;

         for my $line (split(/\n/, $chunk)) {

            my $reg = $log_action->{line};
            $line =~ m/$reg/;

            if(scalar(keys %+) == 0) {
               $self->app->log->debug("Line doesn't match: $line");
               next;
            }

            my %found = %+;

            my ($k_year, $k_mon, $k_day, $k_hour, $k_min, $k_sec) = split(/,/, $log_action->{time});

            my ($r_sec, $r_min, $r_hour, $r_day, $r_mon, $r_year);

            my ($sec,$min,$hour,$mday,$mon,$year,$wday,$yday,$isdst) =
                                                               localtime(time);

            $r_sec  = $k_sec  ? $found{$k_sec}  : $sec;
            $r_min  = $k_min  ? $found{$k_min}  : $min;
            $r_hour = $k_hour ? $found{$k_hour} : $hour;
            $r_day  = $k_day  ? $found{$k_day}  : $mday;
            $r_mon  = $k_mon  ? $found{$k_mon}  : $mon;
            $r_year = $k_year ? $found{$k_year} : $year;

            my $time = mktime($r_sec, $r_min, $r_hour, $r_day, ($mon_map->{$r_mon} || $r_mon), $year);

            my $data = {
               data => {
                  %found,
                  host => $Rex::IO::Service::OS::Daemon::hostname,
                  time => $time,
               },
               tag => $log_action->{tag},
               type => "log",
            };

            $self->app->rexio->send($data);
         }

      });

   }
}

1;
