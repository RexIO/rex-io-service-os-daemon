#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::Monitor::System::Cpu::Usage;

use strict;
use warnings;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   $self->{key} = [
                   "system.cpu.usage.user",
                   "system.cpu.usage.nice",
                   "system.cpu.usage.system",
                   "system.cpu.usage.idle",
                   "system.cpu.usage.iowait",
                   "system.cpu.usage.irq",
                   "system.cpu.usage.softirq",
                   "system.cpu.usage.steal",
                   "system.cpu.usage.guest",
                   "system.cpu.usage.guest_nice",
                  ];

   return $self;
}

sub run {
   my ($self) = @_;

   my $data = $self->get_stat();
   if($data) {
      return $data;
   }

   return "NOT_SUPPORTED";
}

sub get_stat {
   my $self = shift;
   my $ret;

   my $stat_file = "/proc/stat";

   if(-f $stat_file) {
      open(my $s, $stat_file);
      my @lines = <$s>;
      chomp @lines;
      close($s);

      my ($cpu_line) = grep { m/^cpu / } @lines;
      my ($cpu_count) = scalar grep { m/^cpu\d+/ } @lines;

      my (@cpu_values) = split(/\s+/, $cpu_line);
      shift @cpu_values;

      $ret = {
         "system.cpu.count" => $cpu_count,
      };

      my $i = 0;
      for my $itm (@{ $self->{key} }) {
         $ret->{$itm} = $cpu_values[$i];
         $i++;
      }
   }
   else {
      # generate fake data
      $ret = {
         "system.cpu.count" => 8,
      };

      my $i = 0;
      for my $itm (@{ $self->{key} }) {
         $ret->{$itm} = int(rand(1000)),
         $i++;
      }

   }

   return $ret;

}

1;
