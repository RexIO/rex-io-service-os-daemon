#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon::Monitor::System::Load;

use strict;
use warnings;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   $self->{key} = "system.load";

   return $self;
}

sub run {
   my ($self) = @_;
   return get_load_average() * 100;
}

sub get_load_average {
   if(-f "/proc/loadavg") {
      open(my $l, "/proc/loadavg") or die "Unable to get server load \n";
      my $load_avg = <$l>;
      close $l;
      my ($one_min_avg) = split(/\s/, $load_avg);
      return $one_min_avg;
   }
   else {
      my $out = qx{LC_ALL=C uptime};
      chomp $out;
      if($out =~ m/(\d+\.\d+) \d+\.\d+ \d+\.\d+$/) {
         return $1;
      }
      else {
         return "NOT_SUPPORTED";
      }
   }

}

1;
