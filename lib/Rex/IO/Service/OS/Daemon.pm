#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
package Rex::IO::Service::OS::Daemon;

use strict;
use warnings;

use Data::Dumper;
use Mojo::UserAgent;
use Mojo::JSON;
use Mojo::Log;
use Mojo::IOLoop;
use Mojo::IOLoop::Delay;
use Rex::IO::Service::OS::Daemon::MessageBrokerClient;
use Rex::IO::Service::OS::Daemon::Command;
use XML::Simple;

our $VERSION = "0.0.11";

our $hostname = qx{hostname -f};
chomp $hostname;

sub new {
   my $that = shift;
   my $proto = ref($that) || $that;
   my $self = { @_ };

   bless($self, $proto);

   if($self->config->{log}->{file} eq "-") {
      $self->{"log"} = Mojo::Log->new(level => $self->config->{log}->{level});
   }
   else {
      $self->{"log"} = Mojo::Log->new(path => $self->config->{log}->{file}, level => $self->config->{log}->{level});
   }
   $self->{json}  = Mojo::JSON->new;

   $self->log->debug("Initializing UA object (" . $self->config->{rexio}->{server} . ":" . $self->config->{rexio}->{port} . ")");
   $self->{rexio} = Rex::IO::Service::OS::Daemon::MessageBrokerClient->new(app    => $self,
                                                                           server => $self->config->{rexio}->{server},
                                                                           port   => $self->config->{rexio}->{port});

   return $self;
}

sub start {
   my ($self) = @_;

   # hrmpf
   Mojo::IOLoop->timer(3 => sub {
      if(-f "/tmp/fi.xml") {
         $self->log->debug("Sending initial inventory...");
         my $fi = eval { local(@ARGV, $/) = ("/tmp/fi.xml"); <>; };
         my $fi_ref = XMLin($fi);

         rename("/tmp/fi.xml", "/tmp/fi.xml.send");

         my $fdisk_out = qx{LC_ALL=C fdisk -l};
         $fi_ref->{fdisk}   = $fdisk_out;
         $fi_ref->{use_mac} = 1;

         $self->rexio->send({
            type => "hello-service",
            info => $fi_ref,
         });
      }
      else {
         $self->rexio->send({
            type => "hello",
         });
      }

      $self->log->debug("Loading plugins...");

      # starting plugins
      $self->config->{plugins} ||= [];
      for my $plugin (@{$self->config->{plugins}}) {
         my $klass = "Rex::IO::Service::OS::Daemon::Command::$plugin";
         $self->log->debug("   - $plugin");
         eval "use $klass";
         my $p = $klass->new(app => $self);
         $p->start;
      }

      # init listener
      $self->rexio->subscribe(sub {
         $self->process_request(@_);
      });

   });

   #$delay->wait unless Mojo::IOLoop->is_running;
   Mojo::IOLoop->start unless Mojo::IOLoop->is_running;

   # this code part is only readed in case of network connection loss
   # restart connection
   $self = ref($self)->new;
   $self->start;
}

sub process_request {
   my ($self, $msg) = @_;

   $self->log->debug("processing message: $msg");

   eval {
      my $json = Mojo::JSON->decode($msg);
      $self->log->debug("message struct: " . Dumper($json));

      if($json && exists $json->{type}) {
         my $command = Rex::IO::Service::OS::Daemon::Command->create($json->{type}, app => $self);
         my $answer = $command->run($json);

         # send answer
         $self->rexio->send({ok => Mojo::JSON->true, type => "return", return_type => $json->{type}, seq => $json->{seq}, data => $answer});
      }
      else {
         $self->log->debug("Got invalid message.\n   $msg");
      }

      1;
   } or do {
      $self->log->debug("Received unknown message. Maybe processed elsewhere.");
   };


}

sub rexio   { (shift)->{rexio};   }
sub channel { (shift)->{channel}; }
sub log     { (shift)->{"log"};   }
sub json    { (shift)->{json};    }

sub config  {
   my ($self) = @_;

   my @cfg = ("/etc/rex/io/service-daemon.conf", "/usr/local/etc/rex/io/service-daemon.conf", "service-daemon.conf");
   my $cfg;
   for my $file (@cfg) {
      if(-f $file) {
         $cfg = $file;
         last;
      }
   }

   my $config = {};

   if($cfg && -f $cfg) {
      my $content = eval { local(@ARGV, $/) = ($cfg); <>; };
      $config  = eval 'package Rex::IO::Config::Loader;'
                           . "no warnings; $content";

      die "Couldn't load configuration file: $@" if(!$config && $@);
      die "Config file invalid. Did not return HASH reference." if( ref($config) ne "HASH" );

      $config->{user}     ||= "root";
      $config->{group}    ||= "root";
      $config->{pid_file} ||= "/var/run/rex-io-service-daemon.pid";

      return $config;
   }
   else {

      if(-f "/proc/cmdline") {
         open(my $fh, "<", "/proc/cmdline") or die($!);
         my @lines = <$fh>;
         close($fh);
         chomp @lines;

         my ($rexio_server, $port) = ($lines[0] =~ m/REXIO_SERVER=([^:]+):(\d+)/);

         $config = {
            rexio => {
               server => $rexio_server,
               port   => $port, 
            },
            user   => "root",
            group  => "root",

            pid_file => "/var/run/rex-io-service-daemon.pid",

            log   => {
               file  => "-",
               level => "debug",
            },
         };

         return $config;
      }

      print "Can't find configuration file.\n";
      exit 1;
   }
}


1;
