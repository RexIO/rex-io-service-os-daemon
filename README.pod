=pod

=head1 Rex::IO::Service::OS::Daemon - A small message broker client

This is part of the Rex.IO Service OS. This is a small daemon that connects to Rex.IO Server and wait for deployment commands.
