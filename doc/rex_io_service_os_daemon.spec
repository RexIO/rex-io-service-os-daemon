%define perl_vendorlib %(eval "`%{__perl} -V:installvendorlib`"; echo $installvendorlib)
%define perl_vendorarch %(eval "`%{__perl} -V:installvendorarch`"; echo $installvendorarch)

%define real_name Rex-IO-Service-OS-Daemon

Summary: Rex is a tool to ease the execution of commands on multiple remote servers.
Name: rex-io-service-os-daemon
Version: 0.0.11
Release: 1
License: Apache 2.0
Group: Utilities/System
Source: http://search.cpan.org/CPAN/authors/id/J/JF/JFRIED/Rex-IO-Service-OS-Daemon-0.0.11.tar.gz
BuildRoot: %{_tmppath}/%{name}-%{version}-%{release}-root

BuildRequires: perl-Net-SSH2
BuildRequires: perl >= 5.10.1
BuildRequires: perl(ExtUtils::MakeMaker)
#Requires: libssh2 >= 1.2.8 - is included in perl-Net-SSH2 deps
Requires: perl-Mojolicious
Requires: perl >= 5.10.1
Requires: perl-YAML

%description
Rex-IO-Service-OS-Daemon is a monitoring and logstreaming agent for Rex.IO.

%prep
%setup -n %{real_name}-%{version}

%build
%{__perl} Makefile.PL INSTALLDIRS="vendor" PREFIX="%{buildroot}%{_prefix}"
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf %{buildroot}
%{__make} pure_install
mkdir -p ${buildroot}/var/log/rex/io
mkdir -p ${buildroot}/var/run/rex/io
mkdir -p %{buildroot}/etc/rex/io
mkdir -p %{buildroot}/etc/init.d

cat >%{buildroot}/etc/init.d/rex-io-service-os-daemon<<EOF

#! /bin/sh

#
# (c) Jan Gehring <jan.gehring@gmail.com>
# 
# vim: set ts=3 sw=3 tw=0:
# vim: set expandtab:
   
### BEGIN INIT INFO
# Provides:          Rex-IO-Service-OS-Daemon
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: initscript to start rex io service os daemon
# Description:       initscript to start rex io service os daemon
### END INIT INFO


DAEMON=/usr/bin/rex-io-service-os-daemon
PATH=/sbin:/usr/sbin:/bin:/usr/bin:/usr/local/sbin:/usr/local/bin
NAME="Rex-IO-Service-OS-Daemon"

# Exit if the package is not installed
[ -x "\$DAEMON" ] || exit 0

log_daemon_msg()
{
   echo -n \$*
}

log_end_msg()
{
   if [ "\$1" = "0" ]; then
      echo "[ok]"
   else
      echo "[failed]"
   fi
}

#
# Function that starts the daemon/service
#
do_start()
{
	# Return
	#   0 if daemon has been started
	#   1 if daemon was already running
	#   2 if daemon could not be started
	
   \$DAEMON start
	RETVAL="\$?"
	exit "\$RETVAL"
}

#
# Function that stops the daemon/service
#
do_stop()
{
	# Return
	#   0 if daemon has been stopped
	#   1 if daemon was already stopped
	#   2 if daemon could not be stopped
	#   other if a failure occurred
	
   \$DAEMON stop
	RETVAL="\$?"
	exit "\$RETVAL"
}

#
# Function that checks the status of the daemon/service
#
do_status()
{
   \$DAEMON status
   if [ "\$?" = "0" ]; then
      log_daemon_msg "\$NAME is running"
      log_end_msg 0
   else
      log_daemon_msg "\$NAME is NOT running"
      log_end_msg 1
   fi
}

case "\$1" in
  start)
	[ "\$VERBOSE" != no ] && log_daemon_msg "Starting \$NAME"
	do_start
	case "\$?" in
		0|1) [ "\$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "\$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  stop)
	[ "\$VERBOSE" != no ] && log_daemon_msg "Stopping \$NAME"
	do_stop
	case "\$?" in
		0|1) [ "\$VERBOSE" != no ] && log_end_msg 0 ;;
		2) [ "\$VERBOSE" != no ] && log_end_msg 1 ;;
	esac
	;;
  status)
   pidof \$DAEMON >/dev/null 2>&1
   ;;
  restart)
	log_daemon_msg "Restarting \$NAME"
	do_stop
	sleep 1
	do_start
	;;
  *)
	echo "Usage: \$0 {start|stop|status|restart}" >&2
	exit 3
	;;
esac

EOF

chmod 755 %{buildroot}/etc/init.d/rex-io-service-os-daemon

cat >%{buildroot}/etc/rex/io/service-daemon.conf<<EOF
{

   # user under which to run
   user     => "root",
   group    => "root",
   pid_file => "/var/run/rex-io-service-daemon.pid",

   # host where the rexio service is running
   rexio => {
      server => "rexio",
      port   => 5000,
   },

   # which logfile to use
   log   => {
      file  => "/var/log/rex-io-service-daemon.log",
      level => "debug",
   },

   # which performance monitors to load
   monitor => {
      interval => 30, # interval for sending new data
      load => [
         "system.load",
         "system.cpu.usage",
      ],
   },

   # live logfile streaming
   logstream => [
      {
         type => "tail",
         file => "/var/log/messages",
         line => '^(?<month>[A-Za-z]+)\s(?<day>\d+)\s(?<hour>\d+):(?<min>\d+):(?<sec>\d+)\s(?<host>[^\s]+)\s(?<component>[^:]+):\s(?<message>.*)\$',
         time => ",month,day,hour,min,sec",
         tag  => "system.log",
      },
      {
         type => "tail",
         file => "/var/log/secure",
         line => '^(?<month>[A-Za-z]+)\s(?<day>\d+)\s(?<hour>\d+):(?<min>\d+):(?<sec>\d+)\s(?<host>[^\s]+)\s(?<component>[^:]+):\s(?<message>.*)\$',
         time => ",month,day,hour,min,sec",
         tag  => "system.log",
      },

   ],

   # which plugins to load at startup
   plugins => [
      "Monitor",
      "LogStream",
   ],

};


EOF

### Clean up buildroot
find %{buildroot} -name .packlist -exec %{__rm} {} \;

%post

/sbin/chkconfig --add rex-io-service-os-daemon



%clean
%{__rm} -rf %{buildroot}

%files
%defattr(-,root,root, 0755)
%doc META.yml 
#%doc %{_mandir}/*
%{_bindir}/*
%{perl_vendorlib}/*
/etc/rex/io/service-daemon.conf
/etc/init.d/rex-io-service-os-daemon


%changelog

* Fri May 03 2013 Jan Gehring <jan.gehring at, gmail.com> 0.0.11-1
- inital release
