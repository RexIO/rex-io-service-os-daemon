use strict;
use warnings;

use Test::More tests => 2;

use_ok "Rex::IO::Service::OS::Daemon";

my $d = Rex::IO::Service::OS::Daemon->new;
ok($d, "created daemon object");

